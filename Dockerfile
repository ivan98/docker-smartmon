FROM alpine:3.11

RUN set -xe && \
    apk add --no-cache \
    smartmontools \
    tzdata

ENV TZ="Asia/Singapore"

COPY entrypoint.sh /entrypoint.sh

RUN chmod a+x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
CMD ["/usr/sbin/smartd", "--debug"]
