#!/bin/sh

day_of_week=1
day_of_month=01
smart_params="-d sat -a"
cat /dev/null > /etc/smartd.conf

for d in `ls /sys/block`; do
  if [ ${d%%?} = sd ]; then
    model=`cat /sys/block/$d/device/model`

    if [ "${model##WDC}" != "$model" ]; then
      # if model starts with WDC, we want to monitor it
      echo "# $model" >> /etc/smartd.conf
      echo "/dev/$d $smart_params -s (S/../../$day_of_week/03|L/../$day_of_month/./04)" >> /etc/smartd.conf
      day_of_week=$((day_of_week + 1))
      day_of_month=$((day_of_month + 5))
      day_of_month=$(printf %02d $day_of_month)
    fi
    if [ "${model##HGST}" != "$model" ]; then
      # if model starts with HGST, we want to monitor it
      echo "# $model" >> /etc/smartd.conf
      echo "/dev/$d $smart_params -s (S/../../$day_of_week/03|L/../$day_of_month/./04)" >> /etc/smartd.conf
      day_of_week=$((day_of_week + 1))
      day_of_month=$((day_of_month + 5))
      day_of_month=$(printf %02d $day_of_month)
    fi
  fi
done

exec "$@"
